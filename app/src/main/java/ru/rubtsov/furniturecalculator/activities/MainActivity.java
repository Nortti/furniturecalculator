package ru.rubtsov.furniturecalculator.activities;

import android.accounts.Account;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import ru.rubtsov.furniturecalculator.BuildConfig;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.fragments.AboutFragment;
import ru.rubtsov.furniturecalculator.fragments.BaseFragment;
import ru.rubtsov.furniturecalculator.fragments.CountFragment;
import ru.rubtsov.furniturecalculator.fragments.PreferencesFragment;
import ru.rubtsov.furniturecalculator.fragments.ProjectsFragment;
import ru.rubtsov.furniturecalculator.utils.purchase.IabBroadcastReceiver;
import ru.rubtsov.furniturecalculator.utils.purchase.IabHelper;
import ru.rubtsov.furniturecalculator.utils.purchase.IabResult;
import ru.rubtsov.furniturecalculator.utils.purchase.Inventory;
import ru.rubtsov.furniturecalculator.utils.purchase.Purchase;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.FONT;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener,IabBroadcastReceiver.IabBroadcastListener {

    private static final int RC_AUTHORIZE_CONTACTS = 1;
    static final int RC_REQUEST = 10001;
    public static final String SKU_PREMIUM = "paid";
    private static final String TAG = "MainActivity";
    @Nullable @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @BindView(R.id.nav_view) NavigationView mNavigationView;


    @BindString(R.string.app_name) String AppName;
    private FragmentManager fragmentManager;
    private Fragment fragment;
    private GoogleApiClient mGoogleApiClient;
    private Account mAuthorizedAccount;
    private HeaderViewHolder mHolder;
    private MenuItem mPayItem;
    private SharedPreferences mPreferences;
    private boolean isPaid = false;
    private IInAppBillingService mService;
    private ServiceConnection mConnection;
    IabHelper mHelper;
    IabBroadcastReceiver mBroadcastReceiver;
    String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsT+H8wlJIyEcRDULH1Xq8HlbiJK9N9lpzE87ZcJQGluDyaZKiydSE58Hmj2Y5D+r+t4Uy9A+THD9Ay8seCKxdo60LcKQVapzHT72H2rAE1PXn7MUene3wuSxkBKzCX/X+NQ2CkCuLOD22+queDpYpoU0tlD6F3gFgoxxONIFQ+SAetpYgnAt1qhIu2qN7vh+xPbgAeLGLSbpWcaf5mLaGGWc7X9GmTMprcHwfbumUH8ejE7tEYm7PYWoq5HO8a2G/yJ62Fl8CW8N0QsXTbjEmLBaJsdUpCh5LaHS4NcQt+On5CPn4vxPsMfQLdFaAPLe/Ro/QulLfUV62Lf1rkw7/QIDAQAB";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        mHelper = new IabHelper(this,base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                mBroadcastReceiver = new IabBroadcastReceiver(MainActivity.this);
                IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);
                registerReceiver(mBroadcastReceiver, broadcastFilter);
                try {
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    complain("Error querying inventory. Another async operation in progress.");
                }
            }
        });

        if (BuildConfig.FLAVOR.equals("pro")) {
            isPaid = true;
            mPreferences.edit().putBoolean(IS_PAID, true).apply();
        }


        String font = mPreferences.getString(FONT, "fonts/OpenSans.ttf");
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, font, true);
        calligrapher.setFont(mNavigationView, font);

//        mPreferences.edit().putBoolean(IS_PAID,false).apply();
        fragment = null;
        fragmentManager = getFragmentManager();


        setSupportActionBar(mToolbar);

        mToolbar.setTitle(AppName);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();



        isPaid = mPreferences.getBoolean(IS_PAID, false);
        if (mNavigationView != null) {
            Menu m = mNavigationView.getMenu();
            mPayItem = (MenuItem) m.findItem(R.id.nav_pay);


            mPayItem.setVisible(!isPaid);

        }
        if (!isPaid){


            mPayItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    if (menuItem.getItemId() == R.id.nav_pay){
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                        showPaymentDialog();
                        mNavigationView.invalidate();
                    }

                    return true;
                }
            });
        }
        mDrawerLayout.openDrawer(GravityCompat.START);



        mNavigationView.setNavigationItemSelectedListener(this);

        //DatabaseInitUtil.populateSync(AppDatabase.getAppDataBase(this));
        View header = mNavigationView.getHeaderView(0);
        mHolder = new HeaderViewHolder(header);

        mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                mService = IInAppBillingService.Stub.asInterface(iBinder);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                mService = null;
            }
        };

        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");

        this.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);



    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory: " + result);
                return;
            }

            Log.d(TAG, "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            // Do we have the premium upgrade?
            Purchase premiumPurchase = inventory.getPurchase(SKU_PREMIUM);
            isPaid = (premiumPurchase != null && verifyDeveloperPayload(premiumPurchase));
            Log.d(TAG, "User is " + (isPaid ? "PREMIUM" : "NOT PREMIUM"));
            // First find out which subscription is auto renewing


            Log.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };


    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();
        return true;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0){
            fm.popBackStack();
        } else if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void receivedBroadcast() {

    }


    public class HeaderViewHolder {

        @BindView(R.id.accountName) TextView mAccountName;
        //  @BindView(R.id.signIn) SignInButton mSignInButton;

        HeaderViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_AUTHORIZE_CONTACTS) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount googleSignInAccount = result.getSignInAccount();
                mAuthorizedAccount = googleSignInAccount.getAccount();
                mHolder.mAccountName.setText(googleSignInAccount.getDisplayName());

            }
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        mToolbar.setSubtitle(item.getTitle());

        int id = item.getItemId();

        switch (id){
            case R.id.nav_count:
                fragment = new CountFragment();
                break;
            case R.id.nav_projects:
                fragment = new ProjectsFragment();
                break;
            case R.id.nav_base:
                fragment = new BaseFragment();
                break;
            case R.id.nav_settings:
                fragment = new PreferencesFragment();
                break;
            case R.id.nav_about:
                fragment = new AboutFragment();
                break;
        }

        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    void complain(String message) {
        Log.e(TAG, "**** Furniture Calculator Error: " + message);
        alert("Error: " + message);
    }

    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d(TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }

    void showPaymentDialog(){
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setTitle(getString(R.string.paidVersion));
        bld.setMessage(getString(R.string.paidMessage));
        bld.setNeutralButton(getString(R.string.pay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onUpgradeAppButtonClicked();
            }
        });
        bld.create().show();
    }


    public void onUpgradeAppButtonClicked() {
        Log.d(TAG, "Upgrade button clicked; launching purchase flow for upgrade.");


        String payload = "";

        try {
            mHelper.launchPurchaseFlow(this, SKU_PREMIUM, RC_REQUEST,
                    mPurchaseFinishedListener, payload);
        } catch (IabHelper.IabAsyncInProgressException e) {
            complain("Error launching purchase flow. Another async operation in progress.");
        }



    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                complain("Error purchasing: " + result);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                return;
            }

            Log.d(TAG, "Purchase successful.");

if (purchase.getSku().equals(SKU_PREMIUM)) {
                // bought the premium upgrade!
                Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
                alert("Thank you for upgrading to premium!");
                isPaid = true;
                mPreferences.edit().putBoolean(IS_PAID,true).apply();
                mPayItem.setVisible(!isPaid);


}

        }
    };



}