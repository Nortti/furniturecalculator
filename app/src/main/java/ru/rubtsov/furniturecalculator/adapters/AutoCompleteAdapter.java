package ru.rubtsov.furniturecalculator.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.BaseDb;


public class AutoCompleteAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private DatabaseHelper dbHelper;
    private List<BaseDb> mResult = new ArrayList<>();

    public AutoCompleteAdapter(Context context) {
        mContext = context;
        dbHelper = new DatabaseHelper(context);
    }

    @Override
    public int getCount() {
        return mResult.size();
    }

    @Override
    public BaseDb getItem(int index) {
        return mResult.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        ((TextView) convertView.findViewById(android.R.id.text1)).setText(getItem(position).getTitle());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                return null;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                // filter data in UI thread instead of background one because of Realm limitation:
                // the data cannot be passed across threads
                if (constraint != null) {
                    String query = constraint.toString();
                    mResult = filterCountries(query);
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
    }

    @NonNull
    private List<BaseDb> filterCountries(String query) {

        return dbHelper.getBaseListByTitle(query);
    }

}
