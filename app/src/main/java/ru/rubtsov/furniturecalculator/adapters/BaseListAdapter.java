package ru.rubtsov.furniturecalculator.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.BaseDb;
import ru.rubtsov.furniturecalculator.fragments.ProjectResultFragment;
import ru.rubtsov.furniturecalculator.fragments.adding.BaseAddFragment;
import ru.rubtsov.furniturecalculator.viewholder.MaterialHolder;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.BASE;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_UPDATE;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_VIEW;

public class BaseListAdapter extends RecyclerView.Adapter<MaterialHolder> {

    Context mContext;
    List<BaseDb> bases;
    DatabaseHelper dbHelper;
    private boolean isPaid;
    private SharedPreferences mPreferences;

    public BaseListAdapter(Context context, List<BaseDb> bases) {
        mContext = context;
        this.bases = bases;
        dbHelper = new DatabaseHelper(context);
        mPreferences = mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        isPaid = mPreferences.getBoolean(IS_PAID, false);
    }

    @Override
    public MaterialHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.child_view, parent, false);
        return new MaterialHolder(mContext,itemView);
    }

    @Override
    public void onBindViewHolder(MaterialHolder holder, final int position) {
        holder.setChildText(bases.get(position).getTitle());
        holder.setChildPrice(bases.get(position).getPrice());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int id = bases.get(position).getId();
                Bundle bundle = new Bundle();
                bundle.putInt(ID,id);
                bundle.putParcelable(BASE, bases.get(position));
                bundle.putBoolean(IS_VIEW,true);
                BaseAddFragment fragment = new BaseAddFragment();
                fragment.setArguments(bundle);
                ((MainActivity) mContext).getFragmentManager().beginTransaction().replace(R.id.container,fragment,BaseAddFragment.TAG).addToBackStack("").commit();

            }
        });

        if (isPaid){
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final CharSequence[] items = {"Редактировать", "Удалить"};
                    final int id = bases.get(position).getId();
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            switch (item){
                                case 0:
                                    Bundle bundle = new Bundle();
                                    bundle.putInt(ID,id);
                                    bundle.putBoolean(IS_UPDATE,true);
                                    BaseAddFragment fragment = new BaseAddFragment();
                                    fragment.setArguments(bundle);
                                    ((MainActivity) mContext).getFragmentManager().beginTransaction().replace(R.id.container,fragment, BaseAddFragment.TAG).addToBackStack("").commit();
                                    break;
                                case 1:
                                    bases.remove(position);
                                    dbHelper.deleteBase(id);
                                    notifyDataSetChanged();
                                    break;
                            }
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return bases.size();
    }
}
