package ru.rubtsov.furniturecalculator.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.util.Data;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.BaseDb;
import ru.rubtsov.furniturecalculator.fragments.BaseListFragment;
import ru.rubtsov.furniturecalculator.fragments.adding.BaseAddFragment;
import ru.rubtsov.furniturecalculator.viewholder.BaseViewHolder;
import ru.rubtsov.furniturecalculator.viewholder.MaterialHolder;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.BASE;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_UPDATE;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_VIEW;

public class BasesAdapter extends BaseAdapter{

    Context mContext;
    String[] category;
    DatabaseHelper db;
    private boolean isPaid;
    private SharedPreferences mPreferences;

    public BasesAdapter(Context context) {
        mContext = context;
        category = mContext.getResources().getStringArray(R.array.materialType);
        db = new DatabaseHelper(mContext);
        mPreferences = mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        isPaid = mPreferences.getBoolean(IS_PAID, false);
    }

    @Override
    public int getCount() {
        return category.length;
    }

    @Override
    public Object getItem(int i) {
        return category[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final List<BaseDb> bases;
        View grid;
        if (view == null) {
            grid = new View(mContext);
            //LayoutInflater inflater = getLayoutInflater();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            grid = inflater.inflate(R.layout.group_view, viewGroup, false);
        } else {
            grid = (View) view;
        }

        TextView title = (TextView) grid.findViewById(R.id.group_title);
        TextView count = (TextView) grid.findViewById(R.id.child_count);
        title.setText(category[i]);

            bases = db.getBaseListByType(i);

        if (bases != null && bases.size() > 0) {
            count.setText(String.valueOf(bases.size()));

        }
            grid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(ID, i);
                    BaseListFragment fragment = new BaseListFragment();
                    fragment.setArguments(bundle);
                    ((MainActivity) mContext).getFragmentManager().beginTransaction().replace(R.id.container, fragment, BaseListFragment.TAG).addToBackStack("").commit();
                }
            });

        return grid;
    }


}
