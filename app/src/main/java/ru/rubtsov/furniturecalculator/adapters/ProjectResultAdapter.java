package ru.rubtsov.furniturecalculator.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.ProjectDb;
import ru.rubtsov.furniturecalculator.fragments.ProjectResultFragment;
import ru.rubtsov.furniturecalculator.fragments.adding.ProjectsAddFragment;
import ru.rubtsov.furniturecalculator.viewholder.ResultViewHolder;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.BASE;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_UPDATE;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_VIEW;
import static ru.rubtsov.furniturecalculator.utils.Constants.PROJECT;

/**
 * Created by aldis on 06.09.2017.
 */

public class ProjectResultAdapter extends RecyclerView.Adapter<ResultViewHolder>{

    Context mContext;
    List<ProjectDb> mProjectDbs;
    DatabaseHelper dbHelper;
    private boolean isPaid;
    private SharedPreferences mPreferences;

    public ProjectResultAdapter(Context context, List<ProjectDb> projectDbs) {
        mContext = context;
        mProjectDbs = projectDbs;
        dbHelper = new DatabaseHelper(context);
        mPreferences = mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        isPaid = mPreferences.getBoolean(IS_PAID, false);

    }

    @Override
    public ResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_result, parent, false);

        return new ResultViewHolder(mContext,itemView);
    }

    @Override
    public void onBindViewHolder(ResultViewHolder holder, final int position) {
        final ProjectDb projectDb = mProjectDbs.get(position);
        holder.setTitle(projectDb.getTitle());
        holder.setPrice(projectDb.getFullPrice());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int id = projectDb.getId();
                Bundle bundle = new Bundle();
                bundle.putInt(ID,id);
                bundle.putParcelable(PROJECT, projectDb);
                bundle.putBoolean(IS_VIEW,true);
                ProjectsAddFragment fragment = new ProjectsAddFragment();
                fragment.setArguments(bundle);
                ((MainActivity) mContext).getFragmentManager().beginTransaction().replace(R.id.container,fragment,ProjectsAddFragment.TAG).addToBackStack("").commit();

            }
        });

        if (isPaid) {
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final CharSequence[] items = {"Редактировать", "Удалить"};
                    final int id = projectDb.getId();
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            switch (item) {
                                case 0:
                                    Bundle bundle = new Bundle();
                                    bundle.putInt(ID, id);
                                    bundle.putBoolean(IS_UPDATE, true);
                                    ProjectsAddFragment fragment = new ProjectsAddFragment();
                                    fragment.setArguments(bundle);
                                    ((MainActivity) mContext).getFragmentManager().beginTransaction().replace(R.id.container, fragment, ProjectsAddFragment.TAG).addToBackStack("").commit();
                                    break;
                                case 1:
                                    mProjectDbs.remove(position);
                                    dbHelper.deleteResult(id);
                                    notifyDataSetChanged();

                                    break;
                            }
                            dialog.dismiss();

                        }
                    });
                    builder.show();
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mProjectDbs.size();
    }
}