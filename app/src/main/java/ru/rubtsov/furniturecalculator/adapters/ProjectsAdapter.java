package ru.rubtsov.furniturecalculator.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.CountDb;
import ru.rubtsov.furniturecalculator.fragments.CountFragment;
import ru.rubtsov.furniturecalculator.fragments.ProjectResultFragment;
import ru.rubtsov.furniturecalculator.fragments.adding.ProjectsAddFragment;
import ru.rubtsov.furniturecalculator.interfaces.RecyclerOnItemClickListener;
import ru.rubtsov.furniturecalculator.viewholder.ProjectViewHolder;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.COUNT;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_UPDATE;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_VIEW;
import static ru.rubtsov.furniturecalculator.utils.Constants.ITEM;

public class ProjectsAdapter extends RecyclerView.Adapter<ProjectViewHolder> implements RecyclerOnItemClickListener {

    List<CountDb> projectList;
    Context mContext;
    DatabaseHelper dbHelper;
    private boolean isPaid;
    private SharedPreferences mPreferences;


    public ProjectsAdapter(Context context, List<CountDb> projectList) {
        this.mContext = context;
        this.projectList = projectList;
        dbHelper = new DatabaseHelper(context);
        mPreferences = mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        isPaid = mPreferences.getBoolean(IS_PAID, false);
    }

    @Override
    public void onItemClick(View childView, int position) {


    }


    @Override
    public ProjectViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_project, parent, false);


        return new ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProjectViewHolder holder, final int position) {
        final CountDb countDb = projectList.get(position);
        holder.setTitle(countDb.getTitle());
        holder.setName(countDb.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int id = countDb.get_id();
                Bundle bundle = new Bundle();
                bundle.putInt(ID,id);
                bundle.putParcelable(COUNT, countDb);
                bundle.putBoolean(IS_VIEW,true);
                CountFragment fragment = new CountFragment();
                fragment.setArguments(bundle);
                ((MainActivity) mContext).getFragmentManager().beginTransaction().replace(R.id.container,fragment,CountFragment.TAG).addToBackStack("").commit();

            }
        });

if (isPaid) {
    holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            final CharSequence[] items = {"Редактировать", "Удалить"};
            final int id = projectList.get(position).get_id();
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            Bundle bundle = new Bundle();
                            bundle.putInt(ID, id);
                            bundle.putBoolean(IS_UPDATE, true);
                            CountFragment fragment = new CountFragment();
                            fragment.setArguments(bundle);
                            ((MainActivity) mContext).getFragmentManager().beginTransaction().replace(R.id.container, fragment, CountFragment.TAG).addToBackStack("").commit();
                            break;
                        case 1:
                            projectList.remove(position);
                            dbHelper.deleteProject(id);
                            notifyDataSetChanged();
                            break;
                    }
                    dialog.dismiss();

                }
            });
            builder.show();
            return true;
        }
    });
}

        holder.getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(ID,projectList.get(position).get_id());
                bundle.putString(ITEM, projectList.get(position).getTitle());
                ProjectResultFragment fragment = new ProjectResultFragment();
                fragment.setArguments(bundle);
                ((MainActivity) mContext).getFragmentManager().beginTransaction().replace(R.id.container,fragment,ProjectResultFragment.TAG).addToBackStack("").commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return projectList.size();
    }


}
