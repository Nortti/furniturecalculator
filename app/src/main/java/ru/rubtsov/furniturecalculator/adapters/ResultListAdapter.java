package ru.rubtsov.furniturecalculator.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.ProjectDb;
import ru.rubtsov.furniturecalculator.db.model.TypeDb;
import ru.rubtsov.furniturecalculator.viewholder.ResultViewHolder;

public class ResultListAdapter extends BaseAdapter {

    Context mContext;
    List<TypeDb> types;
    String[] category;


    public ResultListAdapter(Context context, List<TypeDb> types) {
        mContext = context;
        this.types = types;
        category = mContext.getResources().getStringArray(R.array.materialType);

    }

    @Override
    public int getCount() {
        return types.size();
    }

    @Override
    public TypeDb getItem(int i) {
        return types.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View grid;
        if (view == null) {
            grid = new View(mContext);
            //LayoutInflater inflater = getLayoutInflater();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            grid = inflater.inflate(R.layout.item_result, viewGroup, false);
        } else {
            grid = (View) view;
        }
        TextView title = (TextView) grid.findViewById(R.id.title);
        TextView count = (TextView) grid.findViewById(R.id.price);
        title.setText(category[i]);
        count.setText(String.format(mContext.getResources().getString(R.string.rub),String.valueOf(types.get(i).getPrice())));

        return grid;
    }
}
