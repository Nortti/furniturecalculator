package ru.rubtsov.furniturecalculator.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.rubtsov.furniturecalculator.db.model.BaseDb;
import ru.rubtsov.furniturecalculator.db.model.CountDb;
import ru.rubtsov.furniturecalculator.db.model.ProjectDb;
import ru.rubtsov.furniturecalculator.db.model.TypeDb;
import ru.rubtsov.furniturecalculator.service.base.LocalBaseService;
import ru.rubtsov.furniturecalculator.service.count.LocalCountService;
import ru.rubtsov.furniturecalculator.service.project.LocalProjectService;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;

public class DatabaseHelper extends SQLiteOpenHelper implements LocalBaseService,
        LocalCountService,
        LocalProjectService{
    Context mContext;
    DatabaseOpenHelper mOpenHelper;
    SharedPreferences mPreferences;
    private boolean isPaid;



    public static final String TAG = "DatabaseHelper";

    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "furniture.db";

    /* Tables */
    public static final String TABLE_BASE = "base";
    public static final String TABLE_COUNT = "count";
    public static final String TABLE_PROJECTS = "projects";

    /* Keys */
    public static final String KEY_ID = "_id";
    public static final String KEY_UID = "uid";
    public static final String KEY_TITLE = "title";
    public static final String KEY_NAME = "name";
    public static final String KEY_PRICE = "price";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_COMMENT = "comment";
    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_IS_PERCENT = "is_percent";
    public static final String KEY_PERCENT = "percent";
    public static final String KEY_IS_NDS = "is_nds";
    public static final String KEY_NDS = "nds";
    public static final String KEY_FULL = "full";
    public static final String KEY_ARTICLE = "article";
    public static final String KEY_UNIT = "unit";
    public static final String KEY_TYPE = "type";


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.mContext = context;
        this.mOpenHelper = new DatabaseOpenHelper(context);
        mPreferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        isPaid = mPreferences.getBoolean(IS_PAID, false);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_COUNT + "(" + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_TITLE + " text," + KEY_NAME + " text,"+ KEY_PRICE + " REAL," + KEY_ADDRESS + " text,"+ KEY_PHONE + " text,"+ KEY_COMMENT + " text" + ")");
        db.execSQL("create table " + TABLE_PROJECTS + "(" + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_TITLE + " text,"+ KEY_TYPE + " INTEGER," + KEY_AMOUNT + " INTEGER," + KEY_PRICE + " REAL," + KEY_UNIT + " INTEGER," + KEY_ARTICLE + " INTEGER," + KEY_IS_PERCENT + " INTEGER," + KEY_PERCENT + " INTEGER," + KEY_IS_NDS + " INTEGER," + KEY_NDS + " INTEGER,"+ KEY_UID + " INTEGER,"+ KEY_COMMENT + " text,"+ KEY_FULL + " REAL" + ")");
        db.execSQL("create table " + TABLE_BASE + "(" + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_TITLE + " text," + KEY_PRICE + " REAL,"+ KEY_ARTICLE + " INTEGER," + KEY_UNIT + " INTEGER,"+ KEY_TYPE + " INTEGER,"+ KEY_COMMENT + " text" + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF IT EXISTS " + TABLE_COUNT);
        db.execSQL("DROP TABLE IF IT EXISTS " + TABLE_PROJECTS);
        db.execSQL("DROP TABLE IF IT EXISTS " + TABLE_BASE);
        onCreate(db);

    }

    @Override
    public void createMaterial(BaseDb baseDb) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.beginTransaction();
            db.insert(TABLE_BASE,null, BaseDb.createContentValues(baseDb.getTitle(),baseDb.getPrice(),baseDb.getArticle(),baseDb.getUnit(),baseDb.getType(),baseDb.getComment()));
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e(TAG,"Error while inserting/updating BaseDb load: " + e);
        } finally {
            db.endTransaction();
        }


    }

    @Override
    public List<BaseDb> getBaseList() {

        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+ TABLE_BASE;

        ArrayList<BaseDb> baseDbs = new ArrayList<>();

        Cursor c = db.rawQuery(selectQuery, null);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return baseDbs;
        }

        if (c.moveToFirst()) {
            do {
                BaseDb baseDb = BaseDb.createWithDBCursor(c);
                baseDbs.add(baseDb);
            } while (c.moveToNext());
        }

        c.close();

        return baseDbs;
    }

    @Override
    public List<BaseDb> getBaseListByType(int type) {
        SQLiteDatabase db = isPaid? getReadableDatabase() : mOpenHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+ TABLE_BASE + " WHERE type = ?";
        String[] params = new String[]{String.valueOf(type)};

        ArrayList<BaseDb> baseDbs = new ArrayList<>();

        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return baseDbs;
        }

        if (c.moveToFirst()) {
            do {
                BaseDb baseDb = BaseDb.createWithDBCursor(c);
                baseDbs.add(baseDb);
            } while (c.moveToNext());
        }

        c.close();

        return baseDbs;
    }

    @Override
    public List<BaseDb> getBaseListByTitle(String title) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT * FROM "+ TABLE_BASE + " WHERE title = ?";
        String[] params = new String[]{title};

        ArrayList<BaseDb> baseDbs = new ArrayList<>();

        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return baseDbs;
        }

        if (c.moveToFirst()) {
            do {
                BaseDb baseDb = BaseDb.createWithDBCursor(c);
                baseDbs.add(baseDb);
            } while (c.moveToNext());
        }

        c.close();

        return baseDbs;
    }

    @Override
    public String getTitleById(int id) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT title FROM "+ TABLE_BASE + " WHERE _id = ?";
        String[] params = new String[]{String.valueOf(id)};

        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return null;
        }

        String title = null;

        if (c.moveToFirst()) {
            do {
                title = c.getString(0);
            } while (c.moveToNext());
        }
        c.close();

        return title;
    }

    @Override
    public int getTypeByTitle(String title) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT type FROM "+ TABLE_BASE + " WHERE title = ?";
        String[] params = new String[]{title};

        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return 0;
        }

        int type = 0;

        if (c.moveToFirst()) {
            do {
                type = c.getInt(0);
            } while (c.moveToNext());
        }
        c.close();
        return type;
    }

    @Override
    public BaseDb getBaseByTitle(String title) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT * FROM "+ TABLE_BASE + " WHERE title = ?";
        String[] params = new String[]{title};
        BaseDb baseDb = null;

        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return baseDb;
        }

        if (c.moveToFirst()) {
            do {
                baseDb = BaseDb.createWithDBCursor(c);
            } while (c.moveToNext());
        }

        c.close();

        return baseDb;
    }

    @Override
    public BaseDb getBaseById(int id) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT * FROM "+ TABLE_BASE + " WHERE _id = ?";
        String[] params = new String[]{String.valueOf(id)};
        BaseDb baseDb = null;

        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return baseDb;
        }

        if (c.moveToFirst()) {
            do {
                baseDb = BaseDb.createWithDBCursor(c);
            } while (c.moveToNext());
        }

        c.close();

        return baseDb;
    }

    @Override
    public void updateBase(int id, BaseDb baseDb, String oldTitle) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        List<Integer> prId = getProjectsIdByTitle(oldTitle);

        ContentValues cv = new ContentValues();
        ContentValues values = new ContentValues();
        try {
            cv.put("title", baseDb.getTitle());
            cv.put("price", baseDb.getPrice());
            cv.put("article", baseDb.getArticle());
            cv.put("unit", baseDb.getUnit());
            cv.put("type", baseDb.getType());
            cv.put("comment", baseDb.getComment());

            for (int i = 0; i < prId.size(); i++) {
                ProjectDb projectDb = getProjectById(prId.get(i));
                double nds = projectDb.getNds()*0.01;
                double percent = projectDb.getPercent()*0.01;
                double fullPrice = projectDb.getAmount()*baseDb.getPrice();
                double value = (fullPrice + (fullPrice*percent));
                double sum = (value + (value * nds));

                values.put("title", baseDb.getTitle());
                values.put("price",baseDb.getPrice());
                values.put("unit",baseDb.getUnit());
                values.put("article",baseDb.getArticle());
                values.put("full", sum);
                db.update(TABLE_PROJECTS,
                        values,
                        "_id = ?",
                        new String[]{String.valueOf(prId.get(i))}
                );
                db.update(TABLE_BASE,
                        cv,
                        "_id = ?",
                        new String[]{String.valueOf(id)}
                );


            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void deleteResult(int id) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            db.delete(TABLE_PROJECTS,"_id = "+ id,null);
        } catch (SQLException e) {
            Toast.makeText(mContext, "Error: "+e, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBase(int id) {
        SQLiteDatabase db = getWritableDatabase();

        String title = getTitleById(id);

        List<Integer> prId = getProjectsIdByTitle(title);
        try {
            if (prId != null && !prId.isEmpty()) {
                for (int i = 0; i < prId.size(); i++) {
                    db.delete(TABLE_PROJECTS, "_id = " + prId.get(i), null);
                }
            }
            db.delete(TABLE_BASE,"_id = "+ id,null);
        } catch (SQLException e) {
            Toast.makeText(mContext, "Error: "+e, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void createCount(CountDb countDb) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.beginTransaction();
            db.insert(TABLE_COUNT,null, CountDb.createContentValues(countDb.getTitle(),countDb.getName(),countDb.getPrice(),countDb.getAddress(),countDb.getPhone(),countDb.getComment()));
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e(TAG,"Error while inserting/updating CountDb load: " + e);
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public List<CountDb> getCountList() {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT * FROM "+ TABLE_COUNT;

        ArrayList<CountDb> countDbs = new ArrayList<>();

        Cursor c = db.rawQuery(selectQuery, null);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return countDbs;
        }

        if (c.moveToFirst()) {
            do {
                CountDb countDb = CountDb.createWithDBCursor(c);
                countDbs.add(countDb);
            } while (c.moveToNext());
        }

        c.close();

        return countDbs;
    }

    @Override
    public CountDb getCountByTitle(String title) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT * FROM "+ TABLE_COUNT + " WHERE title = ?";
        String[] params = new String[]{title};
        CountDb countDb = null;

        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return countDb;
        }

        if (c.moveToFirst()) {
            do {
                countDb = CountDb.createWithDBCursor(c);
            } while (c.moveToNext());
        }

        c.close();

        return countDb;
    }

    @Override
    public CountDb getCountById(int id) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT * FROM "+ TABLE_COUNT + " WHERE _id = ?";
        String[] params = new String[]{String.valueOf(id)};
        CountDb countDb = null;

        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return countDb;
        }

        if (c.moveToFirst()) {
            do {
                countDb = CountDb.createWithDBCursor(c);
            } while (c.moveToNext());
        }

        c.close();

        return countDb;
    }

    @Override
    public void updateCount(int id, CountDb countDb) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();

        try {
            cv.put("title", countDb.getTitle());
            cv.put("name", countDb.getName());
            cv.put("price", countDb.getPrice());
            cv.put("address", countDb.getAddress());
            cv.put("phone", countDb.getPhone());
            cv.put("comment", countDb.getComment());
            db.update(TABLE_COUNT,
                    cv,
                    "_id = ?",
                    new String[]{String.valueOf(id)}
                    );
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void updateCountPrice(int id,double price) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();

        try {
            cv.put("price", price);
            db.update(TABLE_COUNT,
                    cv,
                    "_id = ?",
                    new String[]{String.valueOf(id)}
            );
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

    }

    @Override
    public void createProject(ProjectDb projectDb) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.beginTransaction();
            db.insert(TABLE_PROJECTS,null, ProjectDb.createContentValues(projectDb.getTitle(),
                    projectDb.getType(),
                    projectDb.getAmount(),
                    projectDb.getPrice(),
                    projectDb.getUnit(),
                    projectDb.getArticle(),
                    projectDb.isPercent(),
                    projectDb.getPercent(),
                    projectDb.isNds(),
                    projectDb.getNds(),
                    projectDb.getuId(),
                    projectDb.getComment(),
                    projectDb.getFullPrice()));
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e(TAG,"Error while inserting/updating ProjectDb load: " + e);
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public List<ProjectDb> getProjectList() {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT * FROM "+ TABLE_PROJECTS;

        ArrayList<ProjectDb> projectDbs = new ArrayList<>();

        Cursor c = db.rawQuery(selectQuery, null);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return projectDbs;
        }

        if (c.moveToFirst()) {
            do {
                ProjectDb projectDb = ProjectDb.createWithDBCursor(c);
                projectDbs.add(projectDb);
            } while (c.moveToNext());
        }

        c.close();

        return projectDbs;
    }

    @Override
    public List<ProjectDb> getProjectListById(int uid) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT * FROM "+ TABLE_PROJECTS + " WHERE uid = ?";
        String[] params = new String[]{String.valueOf(uid)};

        ArrayList<ProjectDb> projectDbs = new ArrayList<>();

        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return projectDbs;
        }

        if (c.moveToFirst()) {
            do {
                ProjectDb projectDb = ProjectDb.createWithDBCursor(c);
                projectDbs.add(projectDb);
            } while (c.moveToNext());
        }

        c.close();

        return projectDbs;
    }

    @Override
    public List<TypeDb> getProjectListByType(int id) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT type, SUM(full) FROM "+ TABLE_PROJECTS + " WHERE uid = ? GROUP BY type";
        String[] params = new String[]{String.valueOf(id)};

        List<TypeDb> typeDbs = new ArrayList<>();
        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return typeDbs;
        }

        if (c.moveToFirst()) {
            do {
               TypeDb typeDb = new TypeDb();
                typeDb.setType(c.getInt(0));
                typeDb.setPrice(c.getDouble(1));
                typeDbs.add(typeDb);
            } while (c.moveToNext());
        }

        c.close();

        return typeDbs;
    }

    @Override
    public int getProjectIdByTitle(String title) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT _id FROM "+ TABLE_PROJECTS + " WHERE title = ?";
        String[] params = new String[]{title};
        int id = -1;
        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return id;
        }

        if (c.moveToFirst()) {
            do {
               id = c.getInt(0);
            } while (c.moveToNext());
        }


        c.close();
        return id;
    }

    @Override
    public double getSumByType(int uid) {
        double sum = 0;
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT SUM(full) FROM "+ TABLE_PROJECTS + " WHERE type = ?";

        String[] params = new String[]{String.valueOf(uid)};
        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return 0;
        }

        if (c.moveToFirst()) {
            do {
                sum = c.getDouble(0);
            } while (c.moveToNext());
        }

        c.close();
        return sum;
    }

    @Override
    public List<Integer> getProjectsIdByTitle(String title) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT _id FROM "+ TABLE_PROJECTS + " WHERE title = ?";
        String[] params = new String[]{title};
        List<Integer> ids = new ArrayList<>();

        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return null;
        }

        if (c.moveToFirst()) {
            do {
                int id = c.getInt(0);
                ids.add(id);
            } while (c.moveToNext());
        }

        c.close();
        return ids;
    }

    @Override
    public ProjectDb getProjectById(int id) {
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT * FROM "+ TABLE_PROJECTS + " WHERE _id = ?";
        String[] params = new String[]{String.valueOf(id)};
        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return null;
        }

        ProjectDb projectDb = null;

        if (c.moveToFirst()) {
            do {
                projectDb = ProjectDb.createWithDBCursor(c);
            } while (c.moveToNext());
        }

        c.close();
        return projectDb;
    }

    @Override
    public double getFullPriceSum(int id) {
        double sum = 0;
        SQLiteDatabase db = isPaid ? getReadableDatabase() : mOpenHelper.getWritableDatabase();
        String selectQuery = "SELECT SUM(full) FROM "+ TABLE_PROJECTS + " WHERE uid = ?";
        String[] params = new String[]{String.valueOf(id)};
        Cursor c = db.rawQuery(selectQuery, params);
        if (c == null || c.getCount() == 0) {
            if (c != null) c.close();
            return 0;
        }

        if (c.moveToFirst()) {
            do {
                sum = c.getDouble(0);
            } while (c.moveToNext());
        }

        c.close();
        return sum;
    }

    @Override
    public void deleteProject(int id) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            db.delete(TABLE_COUNT,"_id = "+ id,null);
            db.delete(TABLE_PROJECTS,"uid = "+ id,null);
        } catch (SQLException e) {
            Toast.makeText(mContext, "Error: "+e, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void updateResult(int id,ProjectDb projectDb) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();


        try {
            cv.put("amount", projectDb.getAmount());
            cv.put("is_percent",projectDb.isPercent());
            cv.put("percent", projectDb.getPercent());
            cv.put("is_nds",projectDb.isNds());
            cv.put("nds", projectDb.getNds());
            cv.put("comment", projectDb.getComment());
            cv.put("full", projectDb.getFullPrice());
            db.update(TABLE_PROJECTS,
                    cv,
                    "_id = ?",
                    new String[]{String.valueOf(id)}
            );
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }


    }


}
