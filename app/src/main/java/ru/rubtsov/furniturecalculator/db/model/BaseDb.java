package ru.rubtsov.furniturecalculator.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import static ru.rubtsov.furniturecalculator.utils.Constants.ARTICLE;
import static ru.rubtsov.furniturecalculator.utils.Constants.COMMENT;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.PRICE;
import static ru.rubtsov.furniturecalculator.utils.Constants.TITLE;
import static ru.rubtsov.furniturecalculator.utils.Constants.TYPE;
import static ru.rubtsov.furniturecalculator.utils.Constants.UNIT;

public class BaseDb implements Parcelable {
    private int id;
    private String title;
    private double price;
    private int article;
    private int unit;
    private int type;
    private String comment;

    public BaseDb(int id, String title, double price, int article, int unit, int type, String comment) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.article = article;
        this.unit = unit;
        this.type = type;
        this.comment = comment;
    }

    public BaseDb(String title, double price, int article, int unit, int type, String comment) {
        this.title = title;
        this.price = price;
        this.article = article;
        this.unit = unit;
        this.type = type;
        this.comment = comment;
    }

    protected BaseDb(Parcel in) {
        title = in.readString();
        price = in.readInt();
        article = in.readInt();
        unit = in.readInt();
        type = in.readInt();
        comment = in.readString();
    }

    public static final Creator<BaseDb> CREATOR = new Creator<BaseDb>() {
        @Override
        public BaseDb createFromParcel(Parcel in) {
            return new BaseDb(in);
        }

        @Override
        public BaseDb[] newArray(int size) {
            return new BaseDb[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getArticle() {
        return article;
    }

    public void setArticle(int article) {
        this.article = article;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public static BaseDb createWithDBCursor(Cursor c) {
        BaseDb baseDb = new BaseDb(
                c.getInt(c.getColumnIndex(ID)),
                c.getString((c.getColumnIndex(TITLE))),
                c.getDouble((c.getColumnIndex(PRICE))),
                c.getInt(c.getColumnIndex(ARTICLE)),
                c.getInt(c.getColumnIndex(UNIT)),
                c.getInt(c.getColumnIndex(TYPE)),
                c.getString(c.getColumnIndex(COMMENT)));

        baseDb.updateWithDBCursor(c);
        return baseDb;
    }

    private void updateWithDBCursor(Cursor c) {
        title = c.getString(c.getColumnIndex("title"));
        price = c.getInt(c.getColumnIndex("price"));
        article = c.getInt(c.getColumnIndex("article"));
        unit = c.getInt(c.getColumnIndex("unit"));
        type = c.getInt(c.getColumnIndex("type"));
        comment = c.getString(c.getColumnIndex("comment"));
    }

    public static ContentValues createContentValues(String title, double price, int article, int unit, int type, String comment) {
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("price", price);
        values.put("article", article);
        values.put("unit", unit);
        values.put("type", type);
        values.put("comment", comment);
        return values;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeDouble(price);
        dest.writeInt(article);
        dest.writeInt(unit);
        dest.writeInt(type);
        dest.writeString(comment);
    }

   public static List<BaseDb> getItemsByType(List<BaseDb> bases,int type){
        List<BaseDb> list = new ArrayList<>();
        for (int i = 0; i < bases.size(); i++) {
            if (bases.get(i).getType()==type){
                list.add(bases.get(i));
            }
        }
        return list;
    }

}
