package ru.rubtsov.furniturecalculator.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import static ru.rubtsov.furniturecalculator.utils.Constants.ADDRESS;
import static ru.rubtsov.furniturecalculator.utils.Constants.COMMENT;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.NAME;
import static ru.rubtsov.furniturecalculator.utils.Constants.PHONE;
import static ru.rubtsov.furniturecalculator.utils.Constants.PRICE;
import static ru.rubtsov.furniturecalculator.utils.Constants.TITLE;

public class CountDb implements Parcelable {

    private int _id;
    private String title;
    private String name;
    private double price;
    private String address;
    private String phone;
    private String comment;

    public CountDb(int id, String title, String name, double price, String address, String phone, String comment) {
        this._id = id;
        this.title = title;
        this.name = name;
        this.price = price;
        this.address = address;
        this.phone = phone;
        this.comment = comment;
    }

    public CountDb(String title, String name, double price, String address, String phone, String comment) {
        this.title = title;
        this.name = name;
        this.price = price;
        this.address = address;
        this.phone = phone;
        this.comment = comment;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public static CountDb createWithDBCursor(Cursor c) {
        CountDb countDb = new CountDb(
                c.getInt(c.getColumnIndex(ID)),
                c.getString(c.getColumnIndex(TITLE)),
                c.getString(c.getColumnIndex(NAME)),
                c.getDouble(c.getColumnIndex(PRICE)),
                c.getString(c.getColumnIndex(ADDRESS)),
                c.getString(c.getColumnIndex(PHONE)),
                c.getString(c.getColumnIndex(COMMENT)));
        countDb.updateWithDBCursor(c);
        return countDb;
    }

    private void updateWithDBCursor(Cursor c) {
        title = c.getString(c.getColumnIndex("title"));
        name = c.getString(c.getColumnIndex("name"));
        price = c.getDouble(c.getColumnIndex("price"));
        address = c.getString(c.getColumnIndex("address"));
        phone = c.getString(c.getColumnIndex("phone"));
        comment = c.getString(c.getColumnIndex("comment"));
    }

    public static ContentValues createContentValues(String title, String name, double price, String address, String phone, String comment) {
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("name", name);
        values.put("price", price);
        values.put("address", address);
        values.put("phone", phone);
        values.put("comment", comment);
        return values;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
