package ru.rubtsov.furniturecalculator.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import static ru.rubtsov.furniturecalculator.utils.Constants.AMOUNT;
import static ru.rubtsov.furniturecalculator.utils.Constants.ARTICLE;
import static ru.rubtsov.furniturecalculator.utils.Constants.COMMENT;
import static ru.rubtsov.furniturecalculator.utils.Constants.FULL;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_NDS;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PERCENT;
import static ru.rubtsov.furniturecalculator.utils.Constants.NDS;
import static ru.rubtsov.furniturecalculator.utils.Constants.PERCENT;
import static ru.rubtsov.furniturecalculator.utils.Constants.PRICE;
import static ru.rubtsov.furniturecalculator.utils.Constants.TITLE;
import static ru.rubtsov.furniturecalculator.utils.Constants.TYPE;
import static ru.rubtsov.furniturecalculator.utils.Constants.UID;
import static ru.rubtsov.furniturecalculator.utils.Constants.UNIT;

public class ProjectDb implements Parcelable {

    private int id;
    private String title;
    private int type;
    private int amount;
    private double price;
    private int unit;
    private int article;
    private int isPercent;
    private int percent;
    private int isNds;
    private int nds;
    private int uId;
    private String comment;
    private double fullPrice;

    public ProjectDb(String title, int type,int amount, double price, int unit, int article, int isPercent, int percent, int isNds, int nds, int uId, String comment, double fullPrice) {
        this.title = title;
        this.type = type;
        this.amount = amount;
        this.price = price;
        this.unit = unit;
        this.article = article;
        this.isPercent = isPercent;
        this.percent = percent;
        this.isNds = isNds;
        this.nds = nds;
        this.uId = uId;
        this.comment = comment;
        this.fullPrice = fullPrice;
    }

    public ProjectDb(int id, String title, int type, int amount, double price, int unit, int article, int isPercent, int percent, int isNds, int nds, int uId, String comment, double fullPrice) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.amount = amount;
        this.price = price;
        this.unit = unit;
        this.article = article;
        this.isPercent = isPercent;
        this.percent = percent;
        this.isNds = isNds;
        this.nds = nds;
        this.uId = uId;
        this.comment = comment;
        this.fullPrice = fullPrice;
    }

    protected ProjectDb(Parcel in) {
        id = in.readInt();
        title = in.readString();
        type = in.readInt();
        amount = in.readInt();
        price = in.readDouble();
        unit = in.readInt();
        article = in.readInt();
        isPercent = in.readInt();
        percent = in.readInt();
        isNds = in.readInt();
        nds = in.readInt();
        uId = in.readInt();
        comment = in.readString();
        fullPrice = in.readDouble();
    }

    public static final Creator<ProjectDb> CREATOR = new Creator<ProjectDb>() {
        @Override
        public ProjectDb createFromParcel(Parcel in) {
            return new ProjectDb(in);
        }

        @Override
        public ProjectDb[] newArray(int size) {
            return new ProjectDb[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public int getArticle() {
        return article;
    }

    public void setArticle(int article) {
        this.article = article;
    }

    public int isPercent() {
        return isPercent;
    }

    public void setIsPercent(int isPercent) {
        this.isPercent = isPercent;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int isNds() {
        return isNds;
    }

    public void setIsNds(int isNds) {
        this.isNds = isNds;
    }

    public int getNds() {
        return nds;
    }

    public void setNds(int nds) {
        this.nds = nds;
    }

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getFullPrice() {
        return fullPrice;
    }

    public void setFullPrice(double fullPrice) {
        this.fullPrice = fullPrice;
    }

    public static ProjectDb createWithDBCursor(Cursor c) {
        ProjectDb projectDb = new ProjectDb(
                c.getInt(c.getColumnIndex(ID)),
                c.getString((c.getColumnIndex(TITLE))),
                c.getInt(c.getColumnIndex(TYPE)),
                c.getInt((c.getColumnIndex(AMOUNT))),
                c.getDouble((c.getColumnIndex(PRICE))),
                c.getInt((c.getColumnIndex(UNIT))),
                c.getInt((c.getColumnIndex(ARTICLE))),
                c.getInt(c.getColumnIndex(IS_PERCENT)),
                c.getInt(c.getColumnIndex(PERCENT)),
                c.getInt(c.getColumnIndex(IS_NDS)),
                c.getInt(c.getColumnIndex(NDS)),
                c.getInt(c.getColumnIndex(UID)),
                c.getString(c.getColumnIndex(COMMENT)),
                c.getDouble(c.getColumnIndex(FULL)));

        projectDb.updateWithDBCursor(c);
        return projectDb;
    }

    private void updateWithDBCursor(Cursor c) {
        title = c.getString(c.getColumnIndex("title"));
        amount = c.getInt(c.getColumnIndex("amount"));
        price = c.getDouble(c.getColumnIndex("price"));
        unit = c.getInt(c.getColumnIndex("unit"));
        article = c.getInt(c.getColumnIndex("article"));
        isPercent = c.getInt(c.getColumnIndex("is_percent"));
        percent = c.getInt(c.getColumnIndex("percent"));
        isNds = c.getInt(c.getColumnIndex("is_nds"));
        nds = c.getInt(c.getColumnIndex("nds"));
        uId = c.getInt(c.getColumnIndex("uid"));
        comment = c.getString(c.getColumnIndex("comment"));
        fullPrice = c.getDouble(c.getColumnIndex("full"));
    }

    public static ContentValues createContentValues(String title, int type, int amount, double price, int unit, int article, int isPercent, int percent, int isNds, int nds, int uId, String comment, double fullPrice) {
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("type", type);
        values.put("amount", amount);
        values.put("price",price);
        values.put("unit",unit);
        values.put("article",article);
        values.put("is_percent",isPercent);
        values.put("percent", percent);
        values.put("is_nds",isNds);
        values.put("nds", nds);
        values.put("uid", uId);
        values.put("comment", comment);
        values.put("full", fullPrice);
        return values;
    }

    public static List<ProjectDb> getItemsByType(List<ProjectDb> projects, int type){
        List<ProjectDb> list = new ArrayList<>();
        for (int i = 0; i < projects.size(); i++) {
            if (projects.get(i).getType()==type){
                list.add(projects.get(i));
            }
        }
        return list;
    }

    public static double getSum(List<ProjectDb> projects, int type){
        double sum = 0;
        for (int i = 0; i < projects.size(); i++) {
            if (projects.get(i).getuId()==type){
                sum += projects.get(i).getFullPrice();
            }
        }
        return sum;
    }

    public static double getSumByType(List<ProjectDb> projects, int type){
        double sum = 0;
        for (int i = 0; i < projects.size(); i++) {
            if (projects.get(i).getuId()==type){
                sum = projects.get(i).getFullPrice();
            }
        }
        return sum;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeInt(amount);
        parcel.writeDouble(price);
        parcel.writeInt(unit);
        parcel.writeInt(article);
        parcel.writeInt(isPercent);
        parcel.writeInt(percent);
        parcel.writeInt(isNds);
        parcel.writeInt(nds);
        parcel.writeInt(uId);
        parcel.writeString(comment);
        parcel.writeDouble(fullPrice);
    }
}
