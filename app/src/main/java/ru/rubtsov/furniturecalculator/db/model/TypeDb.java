package ru.rubtsov.furniturecalculator.db.model;

public class TypeDb {

    int type;
    double price;

    public TypeDb() {
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
