package ru.rubtsov.furniturecalculator.fragments;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.adapters.BasesAdapter;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.BaseDb;
import ru.rubtsov.furniturecalculator.fragments.adding.BaseAddFragment;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.FONT;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {


    public static final String TAG = "BaseFragment";
    public static final String LOG = "LOG";
    private String[] category;


    private boolean isPaid;
    private SharedPreferences mPreferences;

    BasesAdapter mAdapter;

    public BaseFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.list_view)
    ListView mListView;
    Map<String, String> map;
    ArrayList<ArrayList<Map<String, String>>> сhildDataList;
    DatabaseHelper dbHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_base, container, false);

        ButterKnife.bind(this,view);

        setHasOptionsMenu(true);

        mPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        isPaid = mPreferences.getBoolean(IS_PAID, false);

        ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(getString(R.string.base));
        // коллекция для групп
        category = getResources().getStringArray(R.array.materialType);

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dbHelper = new DatabaseHelper(getActivity());


        mPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);


        String font = mPreferences.getString(FONT, "fonts/OpenSans.ttf");
        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(getActivity(), font, true);

        if (isPaid){
            mAdapter = new BasesAdapter(getActivity());

            mListView.setAdapter(mAdapter);
        } else {


            mAdapter = new BasesAdapter(getActivity());
            mListView.setAdapter(mAdapter);
        }


    }

/*    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (isPaid) {
            inflater.inflate(R.menu.menu_add, menu);
        }
        super.onCreateOptionsMenu(menu,inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add:
                getActivity().getFragmentManager().beginTransaction().replace(R.id.container,new BaseAddFragment(),BaseAddFragment.TAG).addToBackStack("base").commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/



}
