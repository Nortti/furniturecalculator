package ru.rubtsov.furniturecalculator.fragments;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.adapters.BaseListAdapter;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.BaseDb;
import ru.rubtsov.furniturecalculator.fragments.adding.BaseAddFragment;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.BASE;
import static ru.rubtsov.furniturecalculator.utils.Constants.FONT;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_UPDATE;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_VIEW;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseListFragment extends Fragment {


    public static final String TAG = "BaseListFragment";

    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    private boolean isPaid;
    private SharedPreferences mPreferences;
    private int mId;
    DatabaseHelper dbHelper;
    private String[] category;

    BaseListAdapter mAdapter;
    List<BaseDb> bases;
    public BaseListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base_list, container, false);

        ButterKnife.bind(this,view);

        setHasOptionsMenu(true);

        mPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        isPaid = mPreferences.getBoolean(IS_PAID, false);

        dbHelper = new DatabaseHelper(getActivity());

        String font = mPreferences.getString(FONT, "fonts/OpenSans.ttf");
        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(getActivity(), font, true);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mId = bundle.getInt(ID);
        }
        category = getResources().getStringArray(R.array.materialType);

        ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(category[mId]);

        bases = dbHelper.getBaseListByType(mId);


        mAdapter = new BaseListAdapter(getActivity(),bases);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


            recyclerView.setAdapter(mAdapter);

        // Inflate the layout for this fragment
        return view;
    }




   @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (isPaid) {
            inflater.inflate(R.menu.menu_add, menu);
        }
        super.onCreateOptionsMenu(menu,inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add:
                Bundle bundle = new Bundle();
                bundle.putInt(ID, mId);
                BaseAddFragment fragment = new BaseAddFragment();
                fragment.setArguments(bundle);
                getActivity().getFragmentManager().beginTransaction().replace(R.id.container,fragment, BaseAddFragment.TAG).addToBackStack("base").commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
