package ru.rubtsov.furniturecalculator.fragments;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.CountDb;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.COUNT;
import static ru.rubtsov.furniturecalculator.utils.Constants.FONT;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_UPDATE;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_VIEW;

/**
 * A simple {@link Fragment} subclass.
 */
public class CountFragment extends Fragment {



    public static final String TAG = "CountFragment";

    private int mId = 0;
    private boolean isUpdate = false;
    private boolean isView;
    private boolean isPaid;
    private CountDb countDb;

    @BindView(R.id.title) TextInputEditText mTitle;
    @BindView(R.id.name) TextInputEditText mName;
    @BindView(R.id.adress) TextInputEditText mAdress;
    @BindView(R.id.phone) TextInputEditText mPhone;
    @BindView(R.id.notes) TextInputEditText mComments;
    @BindView(R.id.price) TextView mPrice;
    @BindView(R.id.accept) FloatingActionButton mAccept;

    DatabaseHelper dbHelper;
    private SharedPreferences mPreferences;



    public CountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_count, container, false);
        ButterKnife.bind(this,view);
        ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(getString(R.string.count));

        mPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        isPaid = mPreferences.getBoolean(IS_PAID, false);


        String font = mPreferences.getString(FONT, "fonts/OpenSans.ttf");
        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(getActivity(), font, true);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mId = bundle.getInt(ID);
            isUpdate = bundle.getBoolean(IS_UPDATE);
            isView = bundle.getBoolean(IS_VIEW);
            countDb = bundle.getParcelable(COUNT);
        }

        mPrice.setVisibility(View.GONE);
        dbHelper = new DatabaseHelper(getActivity());

        if (isView){
            ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(getString(R.string.view_project));

            CountDb countDb;
            if (isPaid){
                countDb = dbHelper.getCountById(mId);
            } else {
                countDb = bundle.getParcelable(COUNT);
            }
            updateUI(countDb, true);
        } else if (isUpdate){
            ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(getString(R.string.edit_project));
            CountDb countDb;
            if (isPaid){
                countDb = dbHelper.getCountById(mId);
            } else {
                countDb = bundle.getParcelable(COUNT);
            }
            updateUI(countDb, false);
        }

        if (isPaid && !isView){
            mTitle.setEnabled(true);
            mName.setEnabled(true);
            mPrice.setEnabled(true);
            mAdress.setEnabled(true);
            mPhone.setEnabled(true);
            mComments.setEnabled(true);
        }


            mAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty()){
                    Snackbar.make(v, getString(R.string.check_is_not_empty),Snackbar.LENGTH_LONG).show();
                } else {

                    String title = mTitle.getText().toString();
                    String name = mName.getText().toString();
                    double price = isUpdate && countDb.getPrice() > 0? countDb.getPrice() : 0;
                    String address = mAdress.getText().toString();
                    String phone = mPhone.getText().toString();
                    String comment = mComments.getText().toString();

                    if (isUpdate){
                        countDb = new CountDb(title,name,price,address,phone,comment);

                        dbHelper.updateCount(mId,countDb);
                            getActivity().getFragmentManager().beginTransaction().replace(R.id.container,new ProjectsFragment(),ProjectsFragment.TAG).commit();
                            Snackbar.make(v, getString(R.string.edit),Snackbar.LENGTH_SHORT).show();


                    } else {

                        if (!isEquals()){
                            countDb = new CountDb(title,name,price,address,phone,comment);
                            dbHelper.createCount(countDb);

                            getActivity().getFragmentManager().beginTransaction().replace(R.id.container,new ProjectsFragment(),ProjectsFragment.TAG).commit();
                            Snackbar.make(v, getString(R.string.added),Snackbar.LENGTH_SHORT).show();
                        } else {
                            Snackbar.make(v, getString(R.string.check_is_exists),Snackbar.LENGTH_LONG).show();
                        }

                    }



                }
            }
        });

    return view;
    }

    private void updateUI(CountDb countDb, boolean isView) {
        if (countDb != null) {
            mTitle.setText(countDb.getTitle());
            mName.setText(countDb.getName());
            if (countDb.getPrice() > 0){
                mPrice.setVisibility(View.VISIBLE);
                mPrice.setText(String.format(getResources().getString(R.string.price),countDb.getPrice()));
            }
            mAdress.setText(countDb.getAddress());
            mPhone.setText(countDb.getPhone());
            mComments.setText(countDb.getComment());
        }

        mTitle.setEnabled(!isView);
        mName.setEnabled(!isView);
        mPrice.setEnabled(!isView);
        mAdress.setEnabled(!isView);
        mPhone.setEnabled(!isView);
        mComments.setEnabled(!isView);
        int visibility = isView ? View.INVISIBLE : View.VISIBLE;
        mAccept.setVisibility(visibility);
    }

    private boolean isEmpty(){
        boolean title, name;
        if (TextUtils.isEmpty(mTitle.getText())){
            mTitle.setError(getString(R.string.title_not_null),getActivity().getDrawable(R.drawable.ic_error));
            title = true;
        } else {
            title = false;
        }

        if (TextUtils.isEmpty(mName.getText())){
            mName.setError(getString(R.string.name_not_null),getActivity().getDrawable(R.drawable.ic_error));
            name = true;
        } else {
            name = false;
        }
        boolean isEmpty = title || name;
        Log.d(TAG,String.valueOf(isEmpty));
        return isEmpty;
    }

    private boolean isEquals(){
        CountDb countDb = dbHelper.getCountByTitle(mTitle.getText().toString());
        return countDb != null;
    }

}
