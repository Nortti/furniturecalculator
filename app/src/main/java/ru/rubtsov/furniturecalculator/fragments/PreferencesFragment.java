package ru.rubtsov.furniturecalculator.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.utils.MainApplication;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.FONT;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreferencesFragment extends PreferenceFragment {

    private ListPreference mListPreference;
    private CheckBoxPreference mCheckBoxPreference;
    @BindView(R.id.nav_view) NavigationView mNavigationView;
    private SharedPreferences mPreferences;
    private PowerManager.WakeLock mWakeLock;


    public PreferencesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_data_sync);
        ButterKnife.bind(getActivity());

        mPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);


        String font = mPreferences.getString(FONT, "fonts/OpenSans.ttf");
        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(getActivity(), font, true);

        mListPreference = (ListPreference) getPreferenceManager().findPreference("text_font");
        mListPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                CharSequence[] entries = mListPreference.getEntries();
                String font = null;
                int value = mListPreference.findIndexOfValue(o.toString());
                switch (value){
                    case 0:
                        font = "fonts/OpenSans.ttf";
                        setFont(font);

                        break;
                    case 1:
                        font = "fonts/Roboto.ttf";
                        setFont(font);
                        break;
                    case 2:
                        font = "fonts/Ubuntu.ttf";
                        setFont(font);
                        break;
                    case 3:
                        font = "fonts/Lobster.ttf";
                        setFont(font);
                        break;


                }

                preference.setSummary(entries[value]);
                return true;
            }
        });

        mCheckBoxPreference = (CheckBoxPreference) getPreferenceManager().findPreference("screen_lock");
        mCheckBoxPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                mWakeLock = ((PowerManager) getContext().getSystemService(Context.POWER_SERVICE))
                        .newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, getClass().getName());
                if (mCheckBoxPreference.isChecked()){
                    mWakeLock.acquire();
                } else {
                    mWakeLock.release();
                }

                return false;
            }
        });



    }

    public void setFont(String font){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(FONT,font);
        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(getActivity(), font, true);
        calligrapher.setFont(mNavigationView, font);
        editor.apply();
    }
}
