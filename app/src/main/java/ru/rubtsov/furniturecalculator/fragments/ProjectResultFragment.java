package ru.rubtsov.furniturecalculator.fragments;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.adapters.ProjectResultAdapter;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.ProjectDb;
import ru.rubtsov.furniturecalculator.fragments.adding.ProjectsAddFragment;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.CURRENT;
import static ru.rubtsov.furniturecalculator.utils.Constants.FONT;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;
import static ru.rubtsov.furniturecalculator.utils.Constants.ITEM;
import static ru.rubtsov.furniturecalculator.utils.Constants.REFRESH;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectResultFragment extends Fragment {

    public static final String TAG = "ProjectResultFragment";

    public ProjectResultFragment() {
        // Required empty public constructor
    }
    private int mId;
    private String mTitle;
    List<ProjectDb> projectDbs;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;
    @BindView(R.id.sum) TextView mSum;
    @BindView(R.id.empty_list) TextView mEmptyList;
    SharedPreferences mPreferences;
    DatabaseHelper dbHelper;
    ProjectResultAdapter adapter;
    private boolean isPaid, refresh;
    double sum;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_project_result, container, false);
        ButterKnife.bind(this,view);
        mPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        isPaid = mPreferences.getBoolean(IS_PAID, false);


        String font = mPreferences.getString(FONT, "fonts/OpenSans.ttf");
        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(getActivity(), font, true);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mId = bundle.getInt(ID);
            mTitle = bundle.getString(ITEM, null);
            refresh = bundle.getBoolean(REFRESH);
            mPreferences.edit().putString(CURRENT,mTitle).apply();
            mPreferences.edit().putInt(ID,mId).apply();
        }else {
            mId = mPreferences.getInt(ID,0);
            mTitle  = mPreferences.getString(CURRENT,null);
        }
        ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(mTitle);
        dbHelper = new DatabaseHelper(getActivity());

        projectDbs = dbHelper.getProjectListById(mId);

        setHasOptionsMenu(true);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



            adapter = new ProjectResultAdapter(getActivity(),projectDbs);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

            mRecyclerView.setLayoutManager(layoutManager);

            mRecyclerView.setAdapter(adapter);

            updateSum();
        if (sum > 0){
            mSum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(ID, mId);
                    bundle.putString(ITEM,mTitle);
                    ResultListFragment fragment = new ResultListFragment();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()    ).getFragmentManager().beginTransaction().replace(R.id.container, fragment, ResultListFragment.TAG).addToBackStack("").commit();
                }
            });
        }



    }

    public void updateSum(){
        sum = dbHelper.getFullPriceSum(mId);
        if (sum > 0){
            mSum.setText(String.format(getActivity().getResources().getString(R.string.rub),String.valueOf(String.format("%.2f", sum))));
            if (isPaid) {
                dbHelper.updateCountPrice(mId, sum);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (isPaid){
            inflater.inflate(R.menu.menu_add, menu);
        }

        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.notifyDataSetChanged();
        updateSum();
    }

    @Override
    public void onResume() {
        super.onResume();
            adapter.notifyDataSetChanged();
            updateSum();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add:
                Bundle bundle = new Bundle();
                bundle.putInt(ID,mId);
                ProjectsAddFragment fragment = new ProjectsAddFragment();
                fragment.setArguments(bundle);

                getActivity().getFragmentManager().beginTransaction().replace(R.id.container,fragment, ProjectsAddFragment.TAG).addToBackStack("projectresult").commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void checkEmpty(List<ProjectDb> list){
        if (list.isEmpty()){
            mRecyclerView.setVisibility(View.GONE);
            mEmptyList.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyList.setVisibility(View.GONE);
        }
    }



}
