package ru.rubtsov.furniturecalculator.fragments;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.adapters.ProjectsAdapter;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.CountDb;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.FONT;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectsFragment extends Fragment {


    public static final String TAG = "ProjectsFragment";
    private boolean isPaid;
    private SharedPreferences mPreferences;
    public ProjectsFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;



    private DatabaseHelper dbHelper;
    private ProjectsAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_projects, container, false);

        ButterKnife.bind(this,view);

        mPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        String font = mPreferences.getString(FONT, "fonts/OpenSans.ttf");
        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(getActivity(), font, true);
       //MainApplication.setFont("fonts/Oswald.ttf");

        ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(getString(R.string.projects));
        mPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        isPaid = mPreferences.getBoolean(IS_PAID, false);

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



            dbHelper = new DatabaseHelper(getActivity());
            List<CountDb> countDbs = dbHelper.getCountList();
            adapter = new ProjectsAdapter(getActivity(),countDbs);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(layoutManager);
            mRecyclerView.setAdapter(adapter);


    }


}
