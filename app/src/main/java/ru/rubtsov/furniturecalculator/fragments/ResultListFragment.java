package ru.rubtsov.furniturecalculator.fragments;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.adapters.ResultListAdapter;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.ProjectDb;
import ru.rubtsov.furniturecalculator.db.model.TypeDb;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.CURRENT;
import static ru.rubtsov.furniturecalculator.utils.Constants.FONT;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;
import static ru.rubtsov.furniturecalculator.utils.Constants.ITEM;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultListFragment extends Fragment {


    @BindView(R.id.recycler_view)
    ListView mRecyclerView;

    public static final String TAG = "ResultListFragment";
    SharedPreferences mPreferences;
    DatabaseHelper dbHelper;

    private boolean isPaid;
    private int mId;
    private String mTitle;
    List<TypeDb> mTypeDbs;
    ResultListAdapter adapter;
    String[] category;


    public ResultListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result_list, container, false);

        ButterKnife.bind(this,view);
        mPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        dbHelper = new DatabaseHelper(getActivity());
        category = getActivity().getResources().getStringArray(R.array.materialType);

        isPaid = mPreferences.getBoolean(IS_PAID, false);
        String font = mPreferences.getString(FONT, "fonts/OpenSans.ttf");
        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(getActivity(), font, true);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mId = bundle.getInt(ID);
            mTitle = bundle.getString(ITEM, null);
        }
        ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(mTitle);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mTypeDbs = dbHelper.getProjectListByType(mId);
            adapter = new ResultListAdapter(getActivity(),mTypeDbs);





        mRecyclerView.setAdapter(adapter);
    }


}
