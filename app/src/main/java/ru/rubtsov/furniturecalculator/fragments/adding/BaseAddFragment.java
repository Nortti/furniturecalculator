package ru.rubtsov.furniturecalculator.fragments.adding;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.BaseDb;
import ru.rubtsov.furniturecalculator.fragments.BaseFragment;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.BASE;
import static ru.rubtsov.furniturecalculator.utils.Constants.FONT;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_UPDATE;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_VIEW;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseAddFragment extends Fragment {


    @BindView(R.id.title) TextInputEditText mTitle;
    @BindView(R.id.price) TextInputEditText mPrice;
    @BindView(R.id.article) TextInputEditText mArticle;
    @BindView(R.id.units) Spinner mUnits;
    @BindView(R.id.notes) TextInputEditText mComments;
    @BindView(R.id.typeTxt) TextView mTypeTxt;
    @BindView(R.id.unitsTxt) TextView mUnitsTxt;
    @BindView(R.id.accept) FloatingActionButton mAccept;

    public static final String TAG = "BaseAddFragment";
    private DatabaseHelper dbHelper;
    private int mId = 0;
    private boolean isUpdate = false;
    private boolean isView = false;
    private boolean isPaid;
    private BaseDb baseDb;
    private String oldTitle;
    private SharedPreferences mPreferences;
    String[] units, category;

    public BaseAddFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_base_add, container, false);

        ButterKnife.bind(this,view);

        ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(getString(R.string.add_base_material));

        mPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        isPaid = mPreferences.getBoolean(IS_PAID, false);

        units = getActivity().getResources().getStringArray(R.array.units);
        category = getActivity().getResources().getStringArray(R.array.materialType);
        String font = mPreferences.getString(FONT, "fonts/OpenSans.ttf");
        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(getActivity(), font, true);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mId = bundle.getInt(ID);
            isUpdate = bundle.getBoolean(IS_UPDATE);
            isView = bundle.getBoolean(IS_VIEW);
            baseDb = bundle.getParcelable(BASE);
        }

        //((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //((MainActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

        dbHelper = new DatabaseHelper(getActivity());

        baseDb = dbHelper.getBaseById(mId);
        if (baseDb != null && isUpdate) {
            oldTitle = baseDb.getTitle();
        }

        if (isView){
            ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(getString(R.string.view_material));

            BaseDb base;
            if (isPaid){
                base = dbHelper.getBaseById(mId);
            } else {
                base = bundle.getParcelable(BASE);
            }
            updateUI(base,true);
        } else if (isUpdate){
            ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(getString(R.string.edit_material));

            BaseDb base;
            if (isPaid){
                base = dbHelper.getBaseById(mId);
            } else {
                base = bundle.getParcelable(BASE);
            }
            updateUI(base,false);
        }

        if (isPaid && !isView){
            mTitle.setEnabled(true);
            mPrice.setEnabled(true);
            mArticle.setEnabled(true);
            mUnits.setEnabled(true);
            mTypeTxt.setText(category[mId]);
            mComments.setEnabled(true);
        }

        mAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEmpty()) {
                    Snackbar.make(view, getString(R.string.check_is_not_empty),Snackbar.LENGTH_LONG).show();
                } else {

                    String title = mTitle.getText().toString();
                    int price = Integer.parseInt(mPrice.getText().toString());
                    int article = Integer.parseInt(mArticle.getText().toString());
                    int unit = mUnits.getSelectedItemPosition();
                    int type = mId;
                    String comment = mComments.getText().toString();

                    if (isUpdate){
                        baseDb = new BaseDb(title,price,article,unit,type,comment);
                        dbHelper.updateBase(mId,baseDb,oldTitle);
                        getActivity().getFragmentManager().beginTransaction().replace(R.id.container,new BaseFragment(),BaseFragment.TAG).commit();
                        Snackbar.make(view, getString(R.string.edit),Snackbar.LENGTH_SHORT).show();

                    } else {
                        if (!isEquals()){
                            baseDb = new BaseDb(title,price,article,unit,type,comment);
                            dbHelper .createMaterial(baseDb);
                            Snackbar.make(view, getString(R.string.added),Snackbar.LENGTH_SHORT).show();
                            getActivity().getFragmentManager().beginTransaction().replace(R.id.container,new BaseFragment(),BaseFragment.TAG).commit();
                        } else {
                            Snackbar.make(view, getString(R.string.material_is_exists),Snackbar.LENGTH_LONG).show();
                        }
                    }



                }
            }
        });
        return view;
    }

    private boolean isEmpty(){
        boolean title, price;
        if (TextUtils.isEmpty(mTitle.getText())){
            mTitle.setError(getString(R.string.title_not_null),getActivity().getDrawable(R.drawable.ic_error));
            title = true;
        } else {
            title = false;
        }
        if (TextUtils.isEmpty(mPrice.getText())){
            mPrice.setError(getString(R.string.price_not_null),getActivity().getDrawable(R.drawable.ic_error));
            price = true;
        } else {
            price = false;
        }
        //boolean isEmpty = TextUtils.isEmpty(mTitle.getText()) && TextUtils.isEmpty(mPrice.getText()) && TextUtils.isEmpty(mArticle.getText());
       // Log.d(TAG,"Title"+String.valueOf(TextUtils.isEmpty(mTitle.getText()))+"Price"+String.valueOf(TextUtils.isEmpty(mPrice.getText()))+"Article"+String.valueOf(TextUtils.isEmpty(mArticle.getText())));
        boolean isEmpty = title || price;
        return isEmpty;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateUI(BaseDb baseDb,boolean isView){
        int visibility = isView ? View.INVISIBLE : View.VISIBLE;
        int unitVisibility = isView ? View.VISIBLE : View.GONE;


        if (baseDb != null){
            mTitle.setText(baseDb.getTitle());
            mPrice.setText(String.valueOf(baseDb.getPrice()));
            mArticle.setText(String.valueOf(baseDb.getArticle()));
            mUnits.setSelection(baseDb.getUnit());
            mUnitsTxt.setText(units[baseDb.getUnit()]);
            mTypeTxt.setText(category[baseDb.getType()]);
            mComments.setText(baseDb.getComment());
        }

        mTitle.setEnabled(!isView);
        mPrice.setEnabled(!isView);
        mArticle.setEnabled(!isView);
        mUnits.setVisibility(visibility);
        mUnitsTxt.setVisibility(unitVisibility);
        mComments.setEnabled(!isView);
        mAccept.setVisibility(visibility);
    }

    private boolean isEquals(){
        BaseDb baseDb = dbHelper.getBaseByTitle(mTitle.getText().toString());
        return baseDb != null;
    }
}


