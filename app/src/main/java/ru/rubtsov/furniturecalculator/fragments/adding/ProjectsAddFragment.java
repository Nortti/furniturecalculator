package ru.rubtsov.furniturecalculator.fragments.adding;


import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.activities.MainActivity;
import ru.rubtsov.furniturecalculator.db.DatabaseHelper;
import ru.rubtsov.furniturecalculator.db.model.BaseDb;
import ru.rubtsov.furniturecalculator.db.model.ProjectDb;
import ru.rubtsov.furniturecalculator.fragments.ProjectResultFragment;

import static ru.rubtsov.furniturecalculator.utils.Constants.APP_PREFERENCES;
import static ru.rubtsov.furniturecalculator.utils.Constants.FONT;
import static ru.rubtsov.furniturecalculator.utils.Constants.ID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_PAID;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_UPDATE;
import static ru.rubtsov.furniturecalculator.utils.Constants.IS_VIEW;
import static ru.rubtsov.furniturecalculator.utils.Constants.PROJECT;
import static ru.rubtsov.furniturecalculator.utils.Constants.REFRESH;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectsAddFragment extends Fragment {

    public static final String TAG = "ProjectsAddFragment";

    public ProjectsAddFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.title) AutoCompleteTextView mTitle;
    @BindView(R.id.amount) TextInputEditText mAmount;
    @BindView(R.id.price) TextView mPrice;
    @BindView(R.id.units) TextView mUnits;
    @BindView(R.id.article) TextView mArticle;
    @BindView(R.id.percent) CheckBox mPercent;
    @BindView(R.id.nds) CheckBox mNds;
    @BindView(R.id.percentValue) EditText mPercentValue;
    @BindView(R.id.ndsValue) EditText mNdsValue;
    @BindView(R.id.notes) TextInputEditText mComments;
    @BindView(R.id.accept) FloatingActionButton mAccept;

    private int mId;
    private boolean isUpdate;
    private boolean isView;
    private boolean isPaid;
    private ProjectDb mProjectDb;
    List<BaseDb> mAutoComplete;
    List<BaseDb> mProjects;
    String[] unitValue;
    double sum;
    DatabaseHelper dbHelper;
    private SharedPreferences mPreferences;


    ArrayList<String> result;

    @SuppressLint("StringFormatMatches")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_projects_add, container, false);
        ButterKnife.bind(this,view);

        mPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        isPaid = mPreferences.getBoolean(IS_PAID, false);


        String font = mPreferences.getString(FONT, "fonts/OpenSans.ttf");
        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(getActivity(), font, true);
        dbHelper = new DatabaseHelper(getActivity());
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mId = bundle.getInt(ID);
            isUpdate = bundle.getBoolean(IS_UPDATE);
            isView = bundle.getBoolean(IS_VIEW);
            mProjectDb = bundle.getParcelable(PROJECT);
        }

        unitValue = getActivity().getResources().getStringArray(R.array.units);
        result = new ArrayList<>();
        mAutoComplete = dbHelper.getBaseList();
        mPrice.setText(String.format(getString(R.string.price), "0"));
        mUnits.setText(String.format(getString(R.string.units),"-"));
        mArticle.setText(String.format(getString(R.string.article),0));
        for (int i = 0; i < mAutoComplete.size(); i++) {
            result.add(mAutoComplete.get(i).getTitle());
        }
        mTitle.setAdapter(new ArrayAdapter(getActivity(),android.R.layout.simple_dropdown_item_1line, result));

        mTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkItem();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        if (isView){
            ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(getString(R.string.view_count));

            ProjectDb projectDb;
            if (isPaid){
                projectDb = dbHelper.getProjectById(mId);
            } else {
                projectDb = bundle.getParcelable(PROJECT);
            }
            updateUI(projectDb,true);
        } else if (isUpdate){
            ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(getString(R.string.edit_count));

            ProjectDb projectDb;
            if (isPaid){
                projectDb = dbHelper.getProjectById(mId);
            } else {
                projectDb = bundle.getParcelable(PROJECT);
            }
            updateUI(projectDb,false);
        }

        if (isPaid && !isView){
            mTitle.setEnabled(true);
            mAmount.setEnabled(true);
            mPrice.setEnabled(true);
            mUnits.setEnabled(true);
            mArticle.setEnabled(true);
            mPercent.setEnabled(true);
            mPercentValue.setEnabled(false);
            mNds.setEnabled(true);
            mNdsValue.setEnabled(false);
            mComments.setEnabled(true);
        }

        return view;
    }

    private void updateUI(ProjectDb projectDb, boolean isView) {
        if (projectDb != null) {
            mTitle.setText(projectDb.getTitle());
            mTitle.setEnabled(false);
            mAmount.setText(String.valueOf(projectDb.getAmount()));
            mPrice.setText(String.format(getString(R.string.price), projectDb.getPrice()));
            mUnits.setText(String.format(getString(R.string.units), unitValue[projectDb.getUnit()]));
            mArticle.setText(String.format(getString(R.string.article), projectDb.getArticle()));
            boolean setPercent = projectDb.isPercent() > 0 ? true : false;
            mPercent.setChecked(setPercent);
            if (setPercent){
                mPercentValue.setEnabled(setPercent);
                mPercentValue.setText(String.valueOf(projectDb.getPercent()));
            }

            boolean setNds = projectDb.isNds() > 0 ? true : false;
            mNds.setChecked(setNds);
            if (setNds){
                mNdsValue.setEnabled(setNds);
                mNdsValue.setText(String.valueOf(projectDb.getNds()));
            }
            mComments.setText(projectDb.getComment());


        }

        mTitle.setEnabled(!isView);
        mAmount.setEnabled(!isView);
        mPrice.setEnabled(!isView);
        mUnits.setEnabled(!isView);
        mArticle.setEnabled(!isView);
        mPercent.setEnabled(!isView);
        mPercentValue.setEnabled(!isView);
        mNds.setEnabled(!isView);
        mNdsValue.setEnabled(!isView);
        mComments.setEnabled(!isView);
        int visibility = isView ? View.INVISIBLE : View.VISIBLE;
        mAccept.setVisibility(visibility);
    }

    @OnCheckedChanged(R.id.percent) void percentEnabled(){
        mPercentValue.setEnabled(mPercent.isChecked());
    }

    @OnCheckedChanged(R.id.nds) void ndsEnabled(){
        mNdsValue.setEnabled(mNds.isChecked());
    }
    @SuppressLint("StringFormatMatches")
    private void checkItem(){
        mProjects = dbHelper.getBaseListByTitle(mTitle.getText().toString());
        if (!mProjects.isEmpty()) {
            mPrice.setText(String.format(getString(R.string.price), mProjects.get(0).getPrice()));
            mUnits.setText(String.format(getString(R.string.units), unitValue[mProjects.get(0).getUnit()]));
            mArticle.setText(String.format(getString(R.string.article), mProjects.get(0).getArticle()));
        }


    }

    private boolean isEmpty(){
        boolean title, amount;
        if (TextUtils.isEmpty(mTitle.getText())){
            mTitle.setError(getString(R.string.title_not_null),getActivity().getDrawable(R.drawable.ic_error));
            title = true;
        } else {
            title = false;
        }

        if (TextUtils.isEmpty(mAmount.getText())){
            mAmount.setError(getString(R.string.amount_not_empty),getActivity().getDrawable(R.drawable.ic_error));
            amount = true;
        } else {
            amount = false;
        }
        boolean isEmpty = title || amount;
        return isEmpty;
    }

    @OnClick(R.id.accept) void openBase(){
        if (isEmpty()){
            Snackbar.make(getView(), getString(R.string.check_is_not_empty),Snackbar.LENGTH_LONG).show();
        } else {
            if (!mProjects.isEmpty()){
                double percent;
                double nds;
                double price = mProjects.get(0).getPrice();
                int type = mProjects.get(0).getType();
                int unit = mProjects.get(0).getUnit();
                int article = mProjects.get(0).getArticle();
                int amount = Integer.parseInt(mAmount.getText().toString());
                int isPercent = mPercent.isChecked() ? 1 : 0;
                int isNds = mNds.isChecked() ? 1 : 0;
                double fullPrice = price * amount;
                if (mPercent.isChecked()){
                    percent = Double.parseDouble(mPercentValue.getText().toString())*0.01;
                } else {
                    percent = 1;
                }

                if (mNds.isChecked()){
                    nds = Double.parseDouble(mNdsValue.getText().toString())*0.01;
                } else {
                    nds = 1;
                }

                String title = mTitle.getText().toString();
                int percentVal = Integer.parseInt(mPercentValue.getText().toString());
                int ndsValue = Integer.parseInt(mNdsValue.getText().toString());
                int value = (int) (fullPrice + (fullPrice * percent));
                sum = (value + (value * nds));
                String comment = mComments.getText().toString();



                if (isUpdate){
                    mProjectDb = new ProjectDb(title,type,amount,price,unit, article, isPercent,percentVal,isNds, ndsValue,mId,comment,sum);
                    dbHelper.updateResult(mId,mProjectDb);
                    Snackbar.make(getView(), getString(R.string.edit),Snackbar.LENGTH_SHORT).show();
                    getActivity().getFragmentManager().beginTransaction().replace(R.id.container,new ProjectResultFragment(), ProjectResultFragment.TAG).addToBackStack(null).commit();
                } else {
                    mProjectDb = new ProjectDb(title,type,amount,price,unit, article, isPercent,percentVal,isNds, ndsValue,mId,comment,sum);
                    dbHelper.createProject(mProjectDb);
                    Snackbar.make(getView(), getString(R.string.added),Snackbar.LENGTH_SHORT).show();
                    getActivity().getFragmentManager().beginTransaction().replace(R.id.container,new ProjectResultFragment(), ProjectResultFragment.TAG).addToBackStack(null).commit();
                }


            }
        }


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_add, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }




}