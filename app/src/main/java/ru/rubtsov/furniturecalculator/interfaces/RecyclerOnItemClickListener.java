package ru.rubtsov.furniturecalculator.interfaces;

import android.view.View;

public interface RecyclerOnItemClickListener {

    public void onItemClick(View childView, int position);

}
