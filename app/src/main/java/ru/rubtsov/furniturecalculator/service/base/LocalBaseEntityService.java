package ru.rubtsov.furniturecalculator.service.base;

import java.util.List;

import ru.rubtsov.furniturecalculator.db.model.BaseDb;

public interface LocalBaseEntityService {

    void createMaterial(BaseDb baseDb);

    List<BaseDb> getBaseList();

    List<BaseDb> getBaseListByType(int type);

    List<BaseDb> getBaseListByTitle(String title);

    String getTitleById(int id);

    int getTypeByTitle(String title);

    BaseDb getBaseByTitle(String title);

    BaseDb getBaseById(int id);

    void updateBase(int id, BaseDb baseDb, String oldTitle);

    void deleteBase(int id);
}
