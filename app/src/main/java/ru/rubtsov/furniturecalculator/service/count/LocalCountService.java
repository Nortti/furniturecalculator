package ru.rubtsov.furniturecalculator.service.count;

import java.util.List;

import ru.rubtsov.furniturecalculator.db.model.CountDb;

public interface LocalCountService extends LocalCountEntityService{

    void createCount(CountDb countDb);

    List<CountDb> getCountList();

    CountDb getCountByTitle(String title);

    CountDb getCountById(int id);

    void updateCount(int id, CountDb countDb);

    void updateCountPrice(int id,double price);
}
