package ru.rubtsov.furniturecalculator.service.project;

import java.util.List;

import ru.rubtsov.furniturecalculator.db.model.ProjectDb;
import ru.rubtsov.furniturecalculator.db.model.TypeDb;

public interface LocalProjectService extends LocalProjectEntityService {

    void createProject(ProjectDb projectDb);

    List<ProjectDb> getProjectList();

    List<ProjectDb> getProjectListById(int uid);

    List<TypeDb> getProjectListByType(int id);

    int getProjectIdByTitle(String title);

    double getSumByType(int uid);

    List<Integer> getProjectsIdByTitle(String title);

    ProjectDb getProjectById(int id);

    double getFullPriceSum(int id);

    void deleteProject(int id);

    void updateResult(int id,ProjectDb projectDb);
}
