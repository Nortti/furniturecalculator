package ru.rubtsov.furniturecalculator.utils;

import android.content.SharedPreferences;
import android.preference.Preference;

import me.anwarshahriar.calligrapher.Calligrapher;
import ru.rubtsov.furniturecalculator.activities.MainActivity;

public class Constants {
    public static final String APP_PREFERENCES = "mysettings";

    public static final String IS_PAID = "isPaid";
    public static final String IS_AUTHORIZATED = "isAuthorizated";

    public static final String ID = "_id";
    public static final String UID = "uid";
    public static final String TITLE = "title";
    public static final String PRICE = "price";
    public static final String ARTICLE = "article";
    public static final String UNIT = "unit";
    public static final String TYPE = "type";
    public static final String COMMENT = "comment";
    public static final String NAME = "name";
    public static final String ADDRESS = "address";
    public static final String PHONE = "phone";
    public static final String AMOUNT = "amount";
    public static final String PERCENT = "percent";
    public static final String NDS = "nds";
    public static final String FULL = "full";
    public static final String IS_PERCENT = "is_percent";
    public static final String IS_NDS = "is_nds";


    public static final String ITEM = "item";

    public static final String IS_UPDATE = "is_update";
    public static final String IS_VIEW = "is_view";

    public static final String BASE = "base";
    public static final String PROJECT = "project";
    public static final String COUNT = "count";



    public static final String CURRENT = "current";

    public static final String SKU_LIST = "ITEM_ID_LIST";
    public static final String RESPONSE_CODE = "RESPONSE_CODE";
    public static final String DETAILS_LIST = "DETAILS_LIST";
    public static final int SKU_CODE = 1001;

    public static final String FONT = "font";
    public static final String REFRESH = "refresh";

}
