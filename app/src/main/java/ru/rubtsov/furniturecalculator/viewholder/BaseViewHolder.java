package ru.rubtsov.furniturecalculator.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rubtsov.furniturecalculator.R;

public class BaseViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.group_title) TextView groupTitle;
    @BindView(R.id.child_count) TextView childCount;

    public BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setGroupTitle(String title){
        groupTitle.setText(title);
    }

    public void  setChildCount(int count){
        childCount.setText(String.valueOf(count));
    }
}
