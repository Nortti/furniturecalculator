package ru.rubtsov.furniturecalculator.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rubtsov.furniturecalculator.R;
import ru.rubtsov.furniturecalculator.db.model.BaseDb;

public class MaterialHolder extends RecyclerView.ViewHolder {


    Context mContext;

    @BindView(R.id.title) TextView childText;
    @BindView(R.id.price) TextView childPrice;

    public MaterialHolder(Context context, View itemView) {
        super(itemView);
        this.mContext = context;
        ButterKnife.bind(this,itemView);
    }

    public void onBind(BaseDb baseDb){
        childText.setText(baseDb.getTitle());
        childPrice.setText(String.format(mContext.getResources().getString(R.string.rub),String.valueOf(baseDb.getPrice())));
    }

    public void setChildText(String title) {

        childText.setText(title);
    }

    public void setChildPrice(double price){
        childPrice.setText(String.format(mContext.getResources().getString(R.string.rub),String.valueOf(price)));
    }
}
