package ru.rubtsov.furniturecalculator.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rubtsov.furniturecalculator.R;

public class ProjectViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.name) TextView mName;
    @BindView(R.id.prBut) ImageButton mButton;

    public ProjectViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setTitle(String title){
        mTitle.setText(title);
    }

    public void setName(String name){
        mName.setText(name);
    }

    public ImageButton getButton() {
        return mButton;
    }
}
