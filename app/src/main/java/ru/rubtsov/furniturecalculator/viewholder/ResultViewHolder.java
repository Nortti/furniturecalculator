package ru.rubtsov.furniturecalculator.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rubtsov.furniturecalculator.R;

public class ResultViewHolder extends RecyclerView.ViewHolder {

    Context mContext;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.price) TextView mPrice;

    public ResultViewHolder(Context context,View view) {
        super(view);
        this.mContext = context;
        ButterKnife.bind(this, view);
    }

    public void setTitle(String title){
        mTitle.setText(title);
    }

    public void setPrice(double price){
        mPrice.setText(String.format(mContext.getResources().getString(R.string.rub),String.valueOf(price)));
    }
}